package ru.goloshchapov.tm.entity;

import java.util.Date;

public interface IHaveCreated {

    Date getCreated ();

    void setCreated (Date date);

}
