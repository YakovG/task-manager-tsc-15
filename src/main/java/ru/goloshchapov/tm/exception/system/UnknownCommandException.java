package ru.goloshchapov.tm.exception.system;

import ru.goloshchapov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! Command not found...");
    }

}
