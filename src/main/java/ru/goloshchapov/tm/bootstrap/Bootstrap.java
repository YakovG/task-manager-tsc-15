package ru.goloshchapov.tm.bootstrap;

import ru.goloshchapov.tm.api.*;
import ru.goloshchapov.tm.constant.ArgumentConst;
import ru.goloshchapov.tm.constant.TerminalConst;
import ru.goloshchapov.tm.controller.CommandController;
import ru.goloshchapov.tm.controller.ProjectController;
import ru.goloshchapov.tm.controller.ProjectTaskController;
import ru.goloshchapov.tm.controller.TaskController;
import ru.goloshchapov.tm.exception.system.UnknownArgumentException;
import ru.goloshchapov.tm.exception.system.UnknownCommandException;
import ru.goloshchapov.tm.repository.CommandRepository;
import ru.goloshchapov.tm.repository.ProjectRepository;
import ru.goloshchapov.tm.repository.TaskRepository;
import ru.goloshchapov.tm.service.CommandService;
import ru.goloshchapov.tm.service.ProjectService;
import ru.goloshchapov.tm.service.ProjectTaskService;
import ru.goloshchapov.tm.service.TaskService;
import ru.goloshchapov.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void run(String... args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        System.out.println("USE TEST DATASET? (y)");
        if ("y".equals(TerminalUtil.nextLine())) parseCommand("create-test-data");
        System.out.println("ENTER COMMAND:");
        while (true) {
            try {
                final String command = TerminalUtil.nextLine();
                parseCommand(command);
                System.out.println("[OK]");
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
                System.err.println("[ENTER NEW COMMAND]");
            }
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT: commandController.showAbout(); break;
            case ArgumentConst.ARG_VERSION: commandController.showVersion(); break;
            case ArgumentConst.ARG_HELP: commandController.showHelp(); break;
            case ArgumentConst.ARG_INFO: commandController.showSystemInfo(); break;
            case ArgumentConst.ARG_ARGUMENTS: commandController.showArguments(); break;
            case ArgumentConst.ARG_COMMANDS: commandController.showCommands(); break;
            default: throw new UnknownArgumentException();
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT: commandController.showAbout(); break;
            case TerminalConst.CMD_VERSION: commandController.showVersion(); break;
            case TerminalConst.CMD_HELP: commandController.showHelp(); break;
            case TerminalConst.CMD_INFO: commandController.showSystemInfo(); break;
            case TerminalConst.CMD_COMMANDS: commandController.showCommands(); break;
            case TerminalConst.CMD_ARGUMENTS: commandController.showArguments(); break;
            case TerminalConst.CMD_EXIT: commandController.exit(); break;
            case TerminalConst.CMD_TEST_DATA: projectTaskController.createTestData();break;
            case TerminalConst.TASK_CLEAR: taskController.clear(); break;
            case TerminalConst.TASK_CREATE: taskController.create(); break;
            case TerminalConst.TASK_LIST: taskController.showList(); break;
            case TerminalConst.TASK_SORTED_LIST: taskController.showSortedList(); break;
            case TerminalConst.TASK_VIEW_BY_ID: taskController.showTaskById(); break;
            case TerminalConst.TASK_VIEW_BY_INDEX: taskController.showTaskByIndex(); break;
            case TerminalConst.TASK_VIEW_BY_NAME: taskController.showTaskByName(); break;
            case TerminalConst.TASK_REMOVE_BY_ID: taskController.removeTaskById(); break;
            case TerminalConst.TASK_REMOVE_BY_INDEX: taskController.removeTaskByIndex(); break;
            case TerminalConst.TASK_REMOVE_BY_NAME: taskController.removeTaskByName(); break;
            case TerminalConst.TASK_UPDATE_BY_ID: taskController.updateTaskById(); break;
            case TerminalConst.TASK_UPDATE_BY_INDEX: taskController.updateTaskByIndex(); break;
            case TerminalConst.TASK_START_BY_ID: taskController.startTaskById(); break;
            case TerminalConst.TASK_START_BY_INDEX: taskController.startTaskByIndex(); break;
            case TerminalConst.TASK_START_BY_NAME: taskController.startTaskByName(); break;
            case TerminalConst.TASK_FINISH_BY_ID: taskController.finishTaskById(); break;
            case TerminalConst.TASK_FINISH_BY_INDEX: taskController.finishTaskByIndex(); break;
            case TerminalConst.TASK_FINISH_BY_NAME: taskController.finishTaskByName(); break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID: taskController.changeTaskStatusById(); break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX: taskController.changeTaskStatusByIndex(); break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_NAME: taskController.changeTaskStatusByName(); break;
            case TerminalConst.TASK_LIST_BY_PROJECT_ID: projectTaskController.showAllByProjectId(); break;
            case TerminalConst.TASK_LIST_BY_PROJECT_NAME: projectTaskController.showAllByProjectName(); break;
            case TerminalConst.TASK_LIST_BY_PROJECT_INDEX: projectTaskController.showAllByProjectIndex(); break;
            case TerminalConst.TASK_BIND_BY_PROJECT_ID: projectTaskController.addTaskToProjectByIds(); break;
            case TerminalConst.TASK_UNBIND_BY_PROJECT_ID: projectTaskController.removeTaskFromProjectByIds(); break;
            case TerminalConst.TASK_REMOVE_ALL_BY_PROJECT_ID: projectTaskController.removeAllByProjectId(); break;
            case TerminalConst.TASK_REMOVE_ALL_BY_PROJECT_NAME: projectTaskController.removeAllByProjectName(); break;
            case TerminalConst.TASK_REMOVE_ALL_BY_PROJECT_INDEX: projectTaskController.removeAllByProjectIndex(); break;
            case TerminalConst.PROJECT_CREATE: projectController.create(); break;
            case TerminalConst.PROJECT_LIST: projectController.showList(); break;
            case TerminalConst.PROJECT_SORTED_LIST: projectController.showSortedList(); break;
            case TerminalConst.PROJECT_CLEAR: projectTaskController.clear(); break;
            case TerminalConst.PROJECT_VIEW_BY_ID: projectController.showProjectById(); break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX: projectController.showProjectByIndex(); break;
            case TerminalConst.PROJECT_VIEW_BY_NAME: projectController.showProjectByName(); break;
            case TerminalConst.PROJECT_REMOVE_BY_ID: projectTaskController.removeProjectById(); break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX: projectTaskController.removeProjectByIndex(); break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME: projectTaskController.removeProjectByName(); break;
            case TerminalConst.PROJECT_UPDATE_BY_ID: projectController.updateProjectById(); break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX: projectController.updateProjectByIndex(); break;
            case TerminalConst.PROJECT_START_BY_ID: projectController.startProjectById(); break;
            case TerminalConst.PROJECT_START_BY_INDEX: projectController.startProjectByIndex(); break;
            case TerminalConst.PROJECT_START_BY_NAME: projectController.startProjectByName(); break;
            case TerminalConst.PROJECT_FINISH_BY_ID: projectController.finishProjectById(); break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX: projectController.finishProjectByIndex(); break;
            case TerminalConst.PROJECT_FINISH_BY_NAME: projectController.finishProjectByName(); break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID: projectController.changeProjectStatusById(); break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX: projectController.changeProjectStatusByIndex(); break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME: projectController.changeProjectStatusByName(); break;
            default: throw new UnknownCommandException();
        }
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        try {
            parseArg(arg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

}
