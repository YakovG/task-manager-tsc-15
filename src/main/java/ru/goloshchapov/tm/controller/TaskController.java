package ru.goloshchapov.tm.controller;

import ru.goloshchapov.tm.api.ITaskController;
import ru.goloshchapov.tm.api.ITaskService;
import ru.goloshchapov.tm.enumerated.Sort;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.entity.TaskNotCreatedException;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.exception.entity.TaskNotUpdatedException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println(("STATUS: " + task.getStatus().getDisplayName()));
        System.out.println("CREATED: " + task.getCreated());
        if (task.getDateStart() != null) System.out.println("STARTED: " + task.getDateStart());
        if (task.getDateFinish() != null) System.out.println("FINISHED: " + task.getDateFinish());
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void showSortedList() {
        System.out.println("[TASK SORTED LIST]");
        System.out.println("ENTER SORT TYPE");
        System.out.println(Arrays.toString(Sort.values()));
        System.out.println("DEFAULT TYPE = CREATED");
        final String sortType = TerminalUtil.nextLine();
        final List<Task> tasks = taskService.sortedBy(sortType);
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) throw new TaskNotCreatedException();
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateOneByIndex(index,name,description);
        if (taskUpdated == null) throw new TaskNotUpdatedException();
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateOneById(id,name,description);
        if (taskUpdated == null) throw new TaskNotUpdatedException();
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startTaskByName() {
        System.out.println("[START TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.startTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishTaskById() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishTaskByName() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishTaskByIndex() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.finishTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        final Status [] statuses = Status.values();
        System.out.println(Arrays.toString(statuses));
        final String statusChange = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusById(id, statusChange);
        if (task == null) throw new TaskNotUpdatedException();
    }

    @Override
    public void changeTaskStatusByName() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        final Status [] statuses = Status.values();
        System.out.println(Arrays.toString(statuses));
        final String statusChange = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusByName(name, statusChange);
        if (task == null) throw new TaskNotUpdatedException();
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() -1;
        System.out.println("ENTER STATUS:");
        final Status [] statuses = Status.values();
        System.out.println(Arrays.toString(statuses));
        final String statusChange = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusByIndex(index, statusChange);
        if (task == null) throw new TaskNotUpdatedException();
    }

}
